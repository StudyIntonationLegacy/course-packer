FROM node:8

ENV NODE_ENV development
ENV PORT 8080
ENV MONGO_URL mongodb://localhost:27017/course-editor


WORKDIR /course-packer
ADD . .

RUN npm install

ENTRYPOINT npm start
