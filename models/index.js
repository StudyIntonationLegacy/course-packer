const Course = require("./course"),
      User = require("./user"),
      Lesson = require("./lesson"),
      Task = require("./task"),
      CourseInfo = require("./course-info");

module.exports = {
  User,
  Course,
  Lesson,
  Task,
  CourseInfo
};
