const mongoose = require("mongoose"),
      Schema = mongoose.Schema;

mongoose.set('useFindAndModify', false);

const infoSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    logoUrl: {
      type: String
    },
    difficulty: {
      type: String
    },
    category: {
      type: String
    },
    authors: {
      type: String
    },
    url: {
      type: String
    },
    sha2Hash: {
      type: String
    },
    courseId: {
      type: Schema.Types.ObjectId
    }
  },
  {
    timestamps: true
  }
);

infoSchema.set("toJSON", {
  virtuals: true
});

module.exports = mongoose.model("CourseInfo", infoSchema);
