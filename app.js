const http = require('http'),
      fs = require('fs'),
      sha256 = require('sha256');
const config = require('./config'),
      zip = require('./zip'),
      utils = require('./utils'),
      models = require('./models');

// database
const mongoose = require("mongoose");
mongoose.set("debug", config.DEBUG);
mongoose.connect(
  config.MONGO_URL,
  { useNewUrlParser: true }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function() {
  if (config.DEBUG) {
    console.log("Connection to database open.");
  }
});

// server
const server = http.createServer((req, res) => {
  if ( req.method == 'POST' && req.url == '/course' ) {
    utils.collectRequestData(req, async result => {
      if ( result && result.id) {
        const courseId = result.id;
        if (!courseId.match(/^[0-9a-fA-F]{24}$/)) {
          res.writeHead(404);
          res.end('not found');
        } else {
          const course = await models.Course.findById(courseId);
          if (!course) {
            res.writeHead(404);
            res.end('not found');
          } else {
            await utils.clearFolder('./archive');
            const outputJSON = {
              id: course.id,
              title: course.title,
              description: course.description,
              logo: course.logo,
              difficulty: course.complexity,
              category: course.category,
              authors: course.authors,
              releaseDate: course.createdAt,
              lessons: []
            };

            const lessons = await models.Lesson.find({ course: course.id });
            lessons.forEach(lesson => {
              const lessonJSON = {
                id: lesson.id,
                title: lesson.title,
                numbder: lesson.numbder,
                description: lesson.description,
                duration: lesson.duration,
                tasks: []
              };

              outputJSON.lessons.push(lessonJSON);
            });

            for (var i = 0, len = lessons.length; i < len; i++) {
                const tasks = await models.Task.find({
                  lesson: outputJSON.lessons[i].id
                });

                if (tasks.length) {
                  const taskJSON = {
                    id: tasks[i].id,
                    number: tasks[i].number,
                    instructions: tasks[i].instructions,
                    text: tasks[i].text,
                    sound: tasks[i].sound,
                    pitch: tasks[i].pitch
                  };

                  await utils.getFromS3(tasks[i].pitch);
                  await utils.getFromS3(tasks[i].sound);

                  outputJSON.lessons[i].tasks.push(taskJSON);
                }
            }

            fs.writeFile(`./archive/${courseId}.json`, JSON.stringify(outputJSON, null, ' '), (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            });

            await utils.getFromS3(course.logo);

            const size = await zip.compress(courseId);

            await utils.putToS3(`${courseId}.zip`, fs.readFileSync(`./${courseId}.zip`), 'application/zip');
            
            await models.CourseInfo.findOneAndUpdate({courseId}, {
              title: course.title,
              description: course.description,
              logoUrl: `/uploads/${course.logo}`,
              difficulty: course.complexity,
              category: course.category,
              authors: course.authors,
              url: `/uploads/${courseId}.zip`,
              sha2Hash: sha256(fs.readFileSync(`./${courseId}.zip`)),
              courseId
            }, {upsert: true});

            const courseInfo = await models.CourseInfo.find();
            const courses = [];

            courseInfo.map( item => {
              courses.push({
                id: item.courseId,
                title: item.title,
                description: item.description,
                logoUrl: item.logoUrl,
                size: item.size,
                difficulty: item.difficulty,
                category: item.category,
                version: item.__v,
                sha2Hash: item.sha2Hash,
                url: item.url,
                authors: [item.authors],
                releaseDate: item.updatedAt
              });
            });

            const infoJSON = {'update': new Date(),'courses': courses};
            
            await utils.putToS3('info.json', JSON.stringify(infoJSON), 'application/json');

            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify(infoJSON));
          }
        }              
      } else {
          res.writeHead(400);
          res.end('bad request');            
      }
    });
} else {
    res.writeHead(400);
    res.end('bad request');
}
});
server.listen(config.PORT);
