const fs = require('fs'),
      archiver = require('archiver');


function compress(courseId) {
  const archive = archiver('zip');
  const output = fs.createWriteStream(`./${courseId}.zip`);

  archive.pipe(output);
  archive.directory('archive/', false);
  archive.finalize();

  archive.on('warning', function(err) {
    console.log(err);
    
    if (err.code === 'ENOENT') {
      // log warning
    } else {
      // throw error
      throw err;
    }
  });

  archive.on('error', function(err) {
    throw err;
  });


  return new Promise( (resolve, reject) => {
    output.on('close', function() {
      resolve(archive.pointer());
    });
  });
}

module.exports = ({
  compress
});