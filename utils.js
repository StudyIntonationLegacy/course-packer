
const AWS = require('aws-sdk'),
    { parse } = require('querystring'),
      fs = require('fs');
const config = require('./config');

const s3 = new AWS.S3(config.AWS_PARAM);


function getFromS3(Key) {
    const params = {
        Bucket: config.DESTINATION,
        Key
    };
    const file = fs.createWriteStream(`./archive/${Key}`);
    const fileStream = s3.getObject(params).createReadStream();
    fileStream.pipe(file);
    return new Promise( (resolve, reject) => {
        fileStream.on('end', () => {
            resolve();
        });
    });
}

function putToS3(Key, Body, ContentType) {
    const params = {
        Bucket: config.DESTINATION,
        ACL: 'public-read',
        Key,
        Body,
        ContentType
    };
    return s3.putObject( params ).promise()
}

function clearFolder(path) {
    return new Promise( (resolve, reject) => {
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function(file, index) {
              var curPath = path + "/" + file;
              if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
              } else {
                fs.unlinkSync(curPath);
              }
            });
            resolve();
        } else {
            reject();
        }
    });
}

function collectRequestData(request, callback) {
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    if(request.headers['content-type'] === FORM_URLENCODED) {
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString();
        });
        request.on('end', () => {
            callback(parse(body));
        });
    }
    else {
        callback(null);
    }
}

module.exports = {
    getFromS3,
    putToS3,
    collectRequestData,
    clearFolder
};