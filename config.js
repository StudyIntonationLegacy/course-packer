const dotenv = require("dotenv");
const path = require("path");

const root = path.join.bind(this, __dirname);
dotenv.config({ path: root(".env") });

let mongoURL = "";
if (process.env.NODE_ENV == "test") {
  mongoURL = process.env.TEST_MONGO_URL;
} else {
  mongoURL = process.env.MONGO_URL;
}

const AWS_PARAM = {
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  endpoint: 'scality:8000',
  sslEnabled: false,
  s3ForcePathStyle: true
};

// const serverAdress;

module.exports = {
  MONGO_URL: mongoURL,
  DEBUG: process.env.NODE_ENV === "development",
  PORT: process.env.PORT || 3000,
  DESTINATION: "uploads",
  AWS_PARAM
};
